//
//  BookResult.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

struct BookResult: Model {
    var resultCount: Int?
    var results: [BookItem]?
}
