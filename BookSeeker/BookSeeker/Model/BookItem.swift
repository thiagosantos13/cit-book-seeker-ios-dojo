//
//  BookItem.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

struct BookItem: Model {
    var trackName: String?
    var artworkUrl100: String?
    var artistName: String?
    var description: String?
}
