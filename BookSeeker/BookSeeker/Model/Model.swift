//
//  Model.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

protocol Model: Codable, Equatable {
    
}

extension Model {
    func toData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
}

extension Data {
    func toModel<T: Decodable>() -> T? {
        try? JSONDecoder().decode(T.self, from: self)
    }
    
    func toJson() -> [String: Any]? {
        try? JSONSerialization.jsonObject(with: self, options: .fragmentsAllowed) as? [String: Any]
    }
}
