//
//  BookResultTableViewCell.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import UIKit

class BookResultTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var bookThumbnail: UIImageView!
}
