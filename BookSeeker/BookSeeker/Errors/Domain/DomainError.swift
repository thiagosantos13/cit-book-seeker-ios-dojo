//
//  DomainError.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

enum DomainError: Error {
    case unexpected
}
