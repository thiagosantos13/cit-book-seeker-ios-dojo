//
//  HttpError.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

enum HttpError: Error {
    case noConnectivity
    case badRequest
    case serverError
}
