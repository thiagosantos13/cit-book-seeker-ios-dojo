//
//  HistoryCoreDataService.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

protocol HistoryDataService {
    func addHistory(_ term: String)
    func getAll() -> [HistorySearch]?
    func delete(_ historySearch: HistorySearch)
}
