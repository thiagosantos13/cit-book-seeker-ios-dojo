//
//  HistoryCoreDataService.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import UIKit
import CoreData

final class HistoryCoreDataService: HistoryDataService {
    private var fetchedResultsController: NSFetchedResultsController<HistorySearch>?
    
    private var context: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func addHistory(_ term: String) {
        if let existHistory = getAll()?.filter({ $0.term == term }), existHistory.count > 0 {
            return
        }
        let historySearch = HistorySearch(context: context)
        historySearch.term = term
        historySearch.date = Date()
        try? context.save()
    }
    
    func getAll() -> [HistorySearch]? {
        let searchHistory: NSFetchRequest<HistorySearch> = HistorySearch.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        searchHistory.sortDescriptors = [sortDescriptor]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: searchHistory, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        try? fetchedResultsController?.performFetch()
        return fetchedResultsController?.fetchedObjects
    }
    
    func delete(_ historySearch: HistorySearch) {
        context.delete(historySearch)
        try? context.save()
    }
}
