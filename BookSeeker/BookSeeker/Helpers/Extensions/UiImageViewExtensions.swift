//
//  UiImageViewExtensions.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import UIKit

extension UIImageView {
    func loadImage(withUrl url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let imageData = try? Data(contentsOf: url) {
                if let image = UIImage(data: imageData) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
