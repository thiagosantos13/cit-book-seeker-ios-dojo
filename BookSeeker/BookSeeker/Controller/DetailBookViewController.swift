//
//  DetailBookViewController.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import UIKit

class DetailBookViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    // MARK:- Properties
    var bookItem: BookItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }
    
    private func setupComponents() {
        titleLabel.text = bookItem?.trackName
        customizeTextView()
        addImageFromPath()
    }
}

// MARK:- Private Methods
extension DetailBookViewController {
    private func customizeTextView() {
        if let stringDescription = bookItem?.description?.data(using: .utf8) {
            let attributedString = try? NSAttributedString(data: stringDescription, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            descriptionTextView.attributedText = attributedString
        }
    }
    
    private func addImageFromPath() {
        if let pathImage = bookItem?.artworkUrl100, let imageUrl = URL(string: pathImage) {
            thumbnailImageView.loadImage(withUrl: imageUrl)
        }
    }
}
