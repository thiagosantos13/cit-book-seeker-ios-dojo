//
//  SearchBookViewModel.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

struct SearchBookViewModel: Model {
    var entity: String?
    var term: String?
    
    init(entity: String? = "ibook", term: String?) {
        self.entity = entity
        self.term = term
    }
}
