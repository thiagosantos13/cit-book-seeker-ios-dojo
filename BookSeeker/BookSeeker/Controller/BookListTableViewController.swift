//
//  BookListTableViewController.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import UIKit

class BookListTableViewController: UITableViewController {
    
    var bookList: BookResult?

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateBookList(notification:)), name: .BookListSearched, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- Methods
    @objc func updateBookList(notification: Notification) {
        guard let bookResult = notification.object as? BookResult else { return }
        bookList = bookResult
        tableView.reloadData()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let resultCount = bookList?.resultCount else { return 0 }
        return resultCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BookResultCell", for: indexPath) as? BookResultTableViewCell else { fatalError() }
        let book = bookList?.results?[indexPath.row]
        cell.titleLabel.text = book?.trackName
        cell.authorLabel.text = book?.artistName
        if let pathImage = book?.artworkUrl100, let imageUrl = URL(string: pathImage) {
            cell.bookThumbnail.loadImage(withUrl: imageUrl)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = bookList?.results?[indexPath.row]
        performSegue(withIdentifier: "DetailBookSegue", sender: book)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailBookSegue" {
            guard let detailBookViewController = segue.destination as? DetailBookViewController, let bookItem = sender as? BookItem else { return }
            detailBookViewController.bookItem = bookItem
        }
    }
}
