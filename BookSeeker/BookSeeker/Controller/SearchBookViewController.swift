//
//  ViewController.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import UIKit

class SearchBookViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var searchBarText: UISearchBar!
    @IBOutlet weak var historySearchTableView: UITableView!
    
    // MARK:- Properties
    let searchBookUseCase = SearchBookUseCase(httpClient: AlamofireService())
    let historyCoreDataService = HistoryCoreDataService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBarText.delegate = self
        historySearchTableView.dataSource = self
        historySearchTableView.delegate = self
        searchBarText.searchBarStyle = .minimal
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        historySearchTableView.reloadData()
    }
}

// MARK:- Extensions
extension SearchBookViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        executeUseCase(term: searchBarText.text)
    }
    
    private func showEmptyResultAlert() {
        let alert = UIAlertController(title: "No results found...", message: "No results were found. Please use another term.", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(action)
        self.searchBarText.text = ""
        self.present(alert, animated: true)
    }
    
    private func showErrorResultAlert() {
        let alert = UIAlertController(title: "An error has occurred... :(", message: "Please try again later!", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(action)
        self.searchBarText.text = ""
        self.present(alert, animated: true)
    }
    
    private func executeUseCase(term: String?) {
        searchBookUseCase.execute(searchBookViewModel: SearchBookViewModel(term: term)) { result in
            switch result {
            case .success(let data):
                if let resultCount = data.resultCount, resultCount > 0 {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .BookListSearched, object: data)
                    }
                    self.historyCoreDataService.addHistory(self.searchBarText.text!)
                    self.performSegue(withIdentifier: "BookListSegue", sender: nil)
                } else {
                    self.showEmptyResultAlert()
                }
            case .failure:
                self.showErrorResultAlert()
            }
        }
    }
}

extension SearchBookViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let historyCount = historyCoreDataService.getAll()?.count else { return 0}
        return historyCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryResultCell", for: indexPath)
        guard let history = historyCoreDataService.getAll()?[indexPath.row] else { return cell }
        cell.textLabel?.text = history.term
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let history = historyCoreDataService.getAll()?[indexPath.row] else { return }
            historyCoreDataService.delete(history)
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let history = historyCoreDataService.getAll()?[indexPath.row] else { return }
        executeUseCase(term: history.term)
    }
}

extension Notification.Name {
    static let BookListSearched = Notification.Name("BookListSearched")
}
