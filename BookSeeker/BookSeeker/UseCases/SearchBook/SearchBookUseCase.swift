//
//  SearchBookUseCase.swift
//  BookSeeker
//
//  Created by Thiago Oliveira on 15/11/20.
//

import Foundation

protocol SearchBook {
    func execute(searchBookViewModel: SearchBookViewModel, completion: @escaping (Result<BookResult, DomainError>) -> Void)
}

final class SearchBookUseCase: SearchBook {
    private let url: URL
    private let httpClient: HttpPostClient
    
    init(url: URL = URL(string: "https://itunes.apple.com/search")!, httpClient: HttpPostClient) {
        self.url = url
        self.httpClient = httpClient
    }
    
    func execute(searchBookViewModel: SearchBookViewModel, completion: @escaping (Result<BookResult, DomainError>) -> Void) {
        httpClient.post(to: url, with: searchBookViewModel.toData()) { [weak self] result in
            guard self != nil else { return }
            switch result {
            case .success(let data):
                if let model: BookResult = data?.toModel() {
                    completion(.success(model))
                } else {
                    completion(.failure(.unexpected))
                }
            case .failure:
                completion(.failure(.unexpected))
            }
        }
    }
}
